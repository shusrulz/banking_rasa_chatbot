## happy path
* greet
    - utter_greet
* register_account
    - bank_form
    - form{"name": "bank_form"}
    - form{"name": null}
    - utter_slots_values
* thankyou
    - utter_noworries

## story1
* greet
  - utter_greet

## story3
* bank_location
  - utter_bank_location

## story4
* bank_branch
  - utter_bank_branch

## story5
* bank_time
  - utter_bank_time

## story6
* bank_current_satus
  - utter_bank_current_status

## story7
* bank_working_days
  - utter_bank_working_days

## story8
* get_loan
 - utter_loan

## story9 
* open_account
- utter_open_account

## story10
* international_payment
 - utter_international_payment

## story11
* contact_number
 - utter_contact_number

## story12
* show_rate_of_interest
 - utter_show_interest_rate

## story13
* job_vacancy
 - utter_job_vacancy

##story14
* current_bank_balance
 - utter_bank_balance

##story15
* time_for_bank_loan
 - utter_time_for_bank_loan

##story16
* bank_transaction_detail
 - utter_bank_transaction_detail

##story17
* change_detail
 - utter_change_detail

##story18
* transfer_balance
 - utter_transfer_balance

##story19
* banking_service
 - utter_banking_service

##story20
* get_new_item
 - utter_new_item

##story21
* types_of_loan
 - utter_types_of_loan

##story22
* bank_rank
- utter_bank_rank

##story24
* bank_link
- utter_bank_link

##story25
* account_minimal_balance
- utter_account_balance

##story26
* about_insurance
- utter_about_insurance

## story_new
* about_interest_rate
- utter_about_interest_rate

##story_n
* out_of_scope
- utter_out_of_scope

## story28
* greet
  - utter_greet

## story29
* greet
  - utter_greet

## story30
* greet
  - utter_greet

* open_account{"type_of_account":"checking account"}
- slot{"type_of_account":"checking account"}
- actiontypeofaccount
- slot{"type_of_account":"checking account"}

* open_account{"type_of_account":"saving account"}
- slot{"type_of_account":"saving account"}
- actiontypeofaccount
- slot{"type_of_account":"saving account"}

## story28
* greet
  - utter_greet

* open_account{"type_of_account":"saving account"}
- slot{"type_of_account":"saving account"}
- actiontypeofaccount
- slot{"type_of_account":"saving account"}

* open_account{"type_of_account":"checking account"}
- slot{"type_of_account":"checking account"}
- actiontypeofaccount
- slot{"type_of_account":"checking account"}

## New Story

* greet
 - utter_greet

## New Story

* greet
    - utter_greet
* bank_location
    - utter_bank_location
* bank_branch
    - utter_bank_branch

## Story
* open_account
    - utter_open_account

## New Story

* greet
    - utter_greet
* banking_service
    - utter_banking_service

## New Story

* greet
    - utter_greet
* open_account
    - utter_open_account
* open_account{"type_of_account":"saving account"}
    - slot{"type_of_account":"saving account"}
    - actiontypeofaccount
    - slot{"type_of_account":"saving account"}

## New Story

* greet
    - utter_greet
* open_account
    - utter_open_account
* open_account{"type_of_account":"checking account"}
    - slot{"type_of_account":"checking account"}
    - actiontypeofaccount
    - slot{"type_of_account":"checking account"}

## New Story

* greet
    - utter_greet
* banking_service
    - utter_banking_service
* current_bank_balance
    - utter_bank_balance

## New Story

* current_bank_balance
    - utter_bank_balance

## New Story

* types_of_loan
    - utter_types_of_loan
* get_loan
    - utter_loan

## Story
* open_account
    - utter_open_account
* open_account{"type_of_account":"saving account"}
    - slot{"type_of_account":"saving account"}
    - actiontypeofaccount
    - slot{"type_of_account":"saving account"}
* goodbye
	- utter_goodbye

## New Story

* about
    - utter_about

## New Story

* bank_location
    - utter_bank_location
* bank_branch
    - utter_bank_branch

## New Story

* bank_branch
    - utter_bank_branch
* goodbye
    - utter_goodbye

## New Story

    - utter_greet
* open_account
    - utter_open_account
* open_account{"type_of_account":"saving account"}
    - slot{"type_of_account":"saving account"}
    - actiontypeofaccount
    - slot{"type_of_account":"saving account"}
* goodbye
    - utter_goodbye

## New Story

* greet
    - utter_greet
* bank_location
    - utter_bank_location
* bank_branch
    - utter_bank_branch
* goodbye
    - utter_goodbye
* goodbye
    - utter_goodbye

## New Story

* bank_branch
    - utter_bank_branch
* goodbye
    - utter_goodbye
