## intent:about
- What is this?
- What are you?
- What is this software?
- Is this a chatbot?
- What to do with this software
- What can this software do?
- what can we do here?
- What can this bot do?
- What should i type here?
- Tell me about this chatbot
- tell me about this software
- what is this?

## intent:about_insurance
- Does the bank provide insurance service?
- What are the types of insurance services provided by the bank?

## intent:account_minimal_balance
- How much amount of minimal balance is required in an account?
- What is the minimum balance required in the account?

## intent:account_status
- What is the status of my account?
- Give information of my account
- Please provide the status of my account

## intent:bank_branch
- How many branches do this bank have?
- In how many cities do this bank provide their service?
- Can you show me the list of the bank’s branches?
- In which areas do this bank provide services?
- Give me the list of the bank’s branches.
- How many branches are there?
- How many branches are there in this bank?
- Okay, what about the branches of this bank?

## intent:bank_current_status
- Is the bank open today?
- Is the bank close today?
- Will the bank be providing its services today?
- Is the bank open for transaction?
- Is the bank open for its services?

## intent:bank_link
- Is this bank linked with any international bank?
- Does the bank have any association with foreign bank?
- How can i follow this bank online?
- How to follow this bank in internet?
- provide URL address of this bank
- Does this bank have it's own website?
- How to follow this bank?
- link

## intent:bank_location
- Where is the location of this bank?
- Where is the bank located?
- Show me the direction to the nearest bank.
- Which is the nearest bank from my location?
- Where is the head office of this bank?
- How can I get to this bank?
- Where is your office located?
- WHere is this bank ??

## intent:bank_rank
- In which ranking does this bank fall?
- What is the ranking given to the bank by the government?
- What is the ranking category of the bank?

## intent:bank_share_value
- What is the share value of this bank?
- How much is the value of the bank’s share?
- What is the current price of bank’s share value.

## intent:bank_time
- At what time does the bank open?
- At what time does the bank close?
- What is the closing time of the bank?
- What is the opening time of the bank?
- Show me the opening and closing time of this bank.
- What is the opening time of the bank?
- When does the bank starts?
- When will the bank open?
- Opening time of bank?
- open time
- What is the closing time of the bank?
- When does the bank closes?
- When will the bank close?
- closing time of bank?
- close time
- Is the bank open or closed?
- Bank open closed?

## intent:bank_transaction_detail
- Can you show me my banking transaction?
- Provide me with the details of my banking transaction.
- Show me my banking transaction detail.
- Give me the details of my account’s transaction.
- Show me the transactions that has been occurred so far.

## intent:bank_working_days
- On which days is the bank open?
- What are the working days of the bank?
- On what days are the bank open for customers?
- How many days is the bank providing its services in a week?
- Show me the bank’s working days.

## intent:banking_service
- I want to activate the internet banking.
- How to activate the mobile banking.
- I want to end the internet banking service that I have been using.
- Please end my mobile banking service.
- What are the services of this bank?
- What kind of services are provided by this bank?
- What are the services offered?

## intent:change_detail
- I want to change the nominee of my account?
- What are the process of changing the account nominee?
- I want to change my address.
- How can I change my account type.
- What is the procedure for changing the contact detail?

## intent:contact_number
- Can I get the contact number of the customer service?
- Can I get the contact number of the bank manager?
- Can I get the contact number of the London branch?
- What is the contact number of customer service?
- What is the contact number of bank manager?
- What is the contact number of London branch?

## intent:current_bank_balance
- What is my current bank balance?
- How much amount is there in my account?
- Show me my account’s amount.
- What is the status of my account?
- Give me information on my current bank balance.
- What is my account balance?
- Check my account status

## intent:get_loan
- What are the procedure for getting a loan?
- How can I get a loan?
- Will the bank provide me with the educational loan?
- Will the bank provide me with the home loan?
- Will the bank provide me with the vehicle loan?
- How can I get an educational loan?
- I want to take fixed loan

## intent:get_new_item
- I lost my debit card. How can I get a new one?
- My debit card is broken. I want a new one.
- My cheque book is finished. Order a new one.
- I would like to order a new cheque book.

## intent:goodbye
- bye
- goodbye
- good bye
- stop
- end
- Bye
- Bye
- Bye bye
- Okay, thank you
- Okay i am done.
- Thank you
- Thanks for helping me
- Thanks for helping me
- Stop
- exit
- fuck off
- fuck
- Okay ,thank you
- okay, thank you
- Goodbye

## intent:greet
- hey
- howdy
- hey there
- hello
- hi
- Hello
- Hello, good morning
- hello, good afternoon
- Hello, good evening
- Hey, what's up
- How you doing?
- How is your day?
- Hey
- greetings
- Good morning
- Good afternoon
- Good evening
- Hi
- Hey
- Hey, what's up?

## intent:international_payment
- Can I do any international payment with my debit card?
- Is international payment possible with my debit card?
- Does this debit card support international payment?
- Will I have any trouble using the debit card for international payment?
- Is it possible to use this debit card for international payment transactions?

## intent:job_vacancy
- Are there any job vacancies?
- Is the bank taking any new employees?
- Are there any positions available in the bank?
- Is there any job openings?
- Are there any job opportunities in the bank?

## intent:open_account
- What are the process of opening an account?
- What are the documents required for opening an account?
- How to open an account?
- I want to open an account in this bank.
- Can you help me in opening an account?
- I want to open a [saving account](type_of_account) in this bank
- How to open [saving account](type_of_account) in this bank?
- I want to open [checking account](type_of_account) in this bank
- How to open [checking account](type_of_account) in this bank?
- I want to open a [saving account](type_of_account) in this bank
- How to open [saving account](type_of_account) in this bank?
- I want to open [checking account](type_of_account) in this bank
- How to open [checking account](type_of_account) in this bank?
- I want to open an account
- open an account
- [saving account](type_of_account)
- [checking account](type_of_account)
- How to open account in this bank?

## intent:register_account
- I want to register an account in this bank
- Subodh

## intent:show_rate_of_interest
- What is the rate of interest for fixed deposit?
- What is the rate of interest for loan?
- Show me the rate of interest for all types of loans.
- How much interest will be provided when using fixed deposit account?
- Provide me with the details of rate of interest on all the services.

## intent:time_for_bank_item
- When will I get the cheque book?
- How long does it takes to receive the cheque book after ordering one?
- How much time does it take for the cheque book to be ready?
- When will I receive my debit card?
- When can I come to receive my cheque book and debit card?

## intent:transfer_balance
- I want to transfer an amount in another account.
- Send $600 from my account to another account.

## intent:types_of_loan
- What different types of loan are available?
- What are the different loan services provided by the bank?
- List the types of loan given by the bank.
- What are the types of loan offered?
