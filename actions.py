# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/


# This is a simple example for a custom action which utters "Hello World!"

# from typing import Any, Text, Dict, List
#
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet
from rasa_sdk.forms import FormAction
from typing import Dict, Text, Any, List, Union, Optional



class ActionTypeOfAccount(Action):
	def name(self):
		return 'actiontypeofaccount'

	def run(self,dispatcher,tracker,domain):
		acc_type = tracker.get_slot('type_of_account')
		base_link = "www.examplebankk.com"
		link = base_link + "/" + acc_type
		response = "You can visit following link {} for opening this type of  account".format(link)
		dispatcher.utter_message(response)
		return [SlotSet('type_of_account', acc_type)]

# class AccountInfo(Action):
# 	def name(self):
# 		return 'accountinfo'

# 	def run(self,dispatcher,tracker,domain):
# 		acc_no = tracker.get_slot('account_number')
# 		response = "Your account is disabled"
# 		dispatcher.utter_message(response)
# 		return [SlotSet('account_number', acc_no)]

#
#
# class ActionHelloWorld(Action):
#
#     def name(self) -> Text:
#         return "action_hello_world"
#
#     def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
#
#         dispatcher.utter_message("Hello World!")
#
#         return []

class BankForm(FormAction):
    """Example of a custom form action"""

    def name(self) -> Text:
        """Unique identifier of the form"""

        return "bank_form"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""

        # return ["name", "num_people", "name", "preferences", "account_number"]
        return ["name"]

    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
        """A dictionary to map required slots to
            - an extracted entity
            - intent: value pairs
            - a whole message
            or a list of them, where a first match will be picked"""

        return {
            "name": self.from_entity(entity="name",intent="register_account"),
            "account_number": self.from_entity(entity="account_number",intent="register_account")
        }
        
    
    @staticmethod
    def is_int(string: Text) -> bool:
        """Check if a string is an integer"""

        try:
            int(string)
            return True
        except ValueError:
            return False
    
    
    
    def validate_account_number(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Optional[Text]:
        """Validate account_number value."""

        if self.is_int(value) and int(value) > 0:
            return {"account_number": value}
        else:
            dispatcher.utter_template("utter_wrong_account_number", tracker)
            # validation failed, set slot to None
            return {"account_number": None}

    def validate_name(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Any:
        """Validate name value."""

        if isinstance(value, str):
            if "out" in value:
                # convert "out..." to True
                return {"name": True}
            elif "in" in value:
                # convert "in..." to False
                return {"name": False}
            else:
                dispatcher.utter_template("utter_wrong_name", tracker)
                # validation failed, set slot to None
                return {"name": None}

        else:
            # affirm/deny was picked up as T/F
            return {"name": value}

    def submit(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict]:
        """Define what the form has to do
            after all required slots are filled"""

        # utter submit template
        dispatcher.utter_template("utter_submit", tracker)
        return []
